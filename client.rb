#!/usr/bin/env ruby
#encoding: utf-8

require 'rubyhelper'
require_relative 'nailfucker.meta.rb'
require_relative 'nailfucker_client/nailfucker.client.main.rb'

class NilClass
  def match m, x=nil
    return nil
  end
end

def get_value str
  if str.match /\-?\d+/
    return str.to_i
  elsif str.match /\A(HIGH)|(HARD)\Z/i
    return 20
  elsif str.match /\A(MEDIUM)|(NORMAL)\Z/i
    return 10
  elsif str.match /\Z(LOW)|(WEAK)\Z/i
    return  5
  end

  puts "Error : #{ARGV[0]} is not a valid value"
  return 0
end

def get_hash argv
  i = 0
  h = {
    username: nil,
    password: nil,
    ip: nil,
    port: 4202,
    cmds: []
  }

  while i < argv.size
    if argv[i].match(/\A-p(assword)?\Z/)
      i += 1
      h[:password] = argv[i]
    elsif argv[i].match(/\A-u\Z/)
      i += 1
      h[:username] = argv[i]
    elsif argv[i].match(/\A-ip\Z/)
      i += 1
      h[:ip] = argv[i]
    elsif argv[i].match(/\A-port\Z/)
      i += 1
      h[:port] = argv[i].to_i
    else
      #Commands handler
      if argv[i].match(/\Achgpasswd\Z/)
        if argv[i+1]
          h[:cmds] << argv[i] + " " + argv[i+1]
          i += 1
        else
          puts "Error : no password specified"
          exit(1)
        end
      else
        h[:cmds] << argv[i]
      end
    end
    i += 1
  end
  return h
end

if RUBY_VERSION.to_version < "2.0".to_version
  puts "Your ruby version is not greater or equal than 2.0"
  exit(0)
end

puts "Client " + NailFucker::INFOS
puts

h = get_hash(ARGV)
if h[:ip] == nil
  print "Default ip is 127.0.0.1. Choose an ip or press ENTER : "
  ip = STDIN.gets.to_s.chomp.split(':')
  h[:ip] = ip[0] if ip[0].to_s.size > 0
  h[:port] = ip[1] if ip.size == 2
end

c = NailFucker::Client.new(h[:username], h[:password], h[:ip], h[:port], h[:cmds])
c.connect
#c.close
