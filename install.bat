@echo off

echo You must have RUBY 2.0 or Greater installed
echo -----------------------------------

echo Install BUNDLER
gem install bundler

echo Install DEPENDENCIES
gem install version
gem install rubyhelper
bundle install

echo You can run the client now !
