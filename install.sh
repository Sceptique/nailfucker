#!/usr/bin/bash

which ruby > /dev/null
if [ "$?" != "0" ]
then
    echo "Error occured !"
    echo "Ruby >= 2 not found. Please install Ruby >= 2"
    exit
fi

echo "Install BUNDLER"
gem install bundler
if [ "$?" != "0" ]
then
    echo "Error occured !"
    exit
fi
echo
echo "Install DEPENDENCIES"

bundle install
if [ "$?" != "0" ]
then
    echo "Error occured !"
    exit
fi
echo
echo "You can run the client now !"
