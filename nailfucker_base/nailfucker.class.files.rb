#encoding: utf-8

require 'rubyhelper'

module NailFucker
  class Base

    # Write the current configuration
    def self.write_config
      f = NailFucker::Base.open_config('w')
      @@config.each do |k, v|
        f << k << "=" << v << "\n"
      end
      f.close
      return @@config
    end

    # Write a log line (date + time + log_message)
    def self.write_log log_line
      f = NailFucker::Base.open_log('a')
      f << log_line
      f.close
      return log_line
    end

    #read the config file and save it in @@config
    def self.read_account
      @@account = []
      f = NailFucker::Base.open_account('r')
      while line = f.gets do
        @@account << Account.new(line)
      end
      f.close
      return @@account
    end

    #read the config file
    def self.read_config(custom_file=nil)
      config = {}
      f = NailFucker::Base.open_config('r') if custom_file == nil
      f = NailFucker::Base.open_file("", custom_file, 'r') unless custom_file == nil
      while line = f.gets do
        line = line.split('=')
        config[line[0].to_sym] = line[1..-1].join('=').chomp if line.size >= 2
      end
      f.close
      return config
    end

  end
end
