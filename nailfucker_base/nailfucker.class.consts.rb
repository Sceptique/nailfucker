#encoding: utf-8

module NailFucker
  class Base

    ##FILES
    ROOT_DIR_NAME = Dir.home + "/.config/"
    #CONFIG
    CONFIG_DIR_NAME = ROOT_DIR_NAME + "NailFucker"
    CONFIG_FILE_NAME = "config.conf"
    CONFIG_FILE_PATH = CONFIG_DIR_NAME + "/" + CONFIG_FILE_NAME
    #LOG
    LOG_DIR_NAME = ROOT_DIR_NAME + "NailFucker"
    LOG_FILE_NAME = "journal.log"
    LOG_FILE_PATH = LOG_DIR_NAME + "/" + LOG_FILE_NAME
    #ACCOUNT
    ACCOUNT_DIR_NAME = ROOT_DIR_NAME + "NailFucker"
    ACCOUNT_FILE_NAME = "accounts"
    ACCOUNT_FILE_PATH = ACCOUNT_DIR_NAME + "/" + ACCOUNT_FILE_NAME

    ## OTHERS
    #count
    LOW = 5
    MEDIUM = 10
    HIGH = 20

  end
end
