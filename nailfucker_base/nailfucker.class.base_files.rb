#encoding: utf-8

require 'fileutils'

module NailFucker
  class Base

    # Open a file properly
    def self.open_file dir_name, file_path, mod
      FileUtils.mkdir_p(dir_name)
      File.open(file_path, "a").close if !File.exists?(file_path)
      return File.open(file_path, mod)
    end

    #open the config file
    def self.open_config mod
      return self.open_file(CONFIG_DIR_NAME, CONFIG_FILE_PATH, mod)
    end

    #open the log file
    def self.open_log mod
      return self.open_file(LOG_DIR_NAME, LOG_FILE_PATH, mod)
    end

    #open the log file
    def self.open_account mod
      return self.open_file(ACCOUNT_DIR_NAME, ACCOUNT_FILE_PATH, mod)
    end

  end
end
