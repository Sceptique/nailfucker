#encoding: utf-8

module NailFucker
  class Account

    attr_accessor :username, :password, :count, :admin
    attr_reader :id

    def initialize(in_={})
      @admin = false
      if in_.is_a?Hash
        @id = in_[:id]
        @username = in_[:username]
        @password = in_[:password]
        @count = in_[:count]
      else
        in_ = in_.split(':')if in_.is_a?String
        if in_.is_a?Array and in_.size == 4
          @id = in_[0].to_i
          @username = in_[1]
          @password = in_[2]
          @count = in_[3].to_i
        end
      end
    end

    def self.find_by_id id
      f = NailFucker::Base.open_account('r')
      while line = f.gets do
        a = line.split(':')
        return NailFucker::Account.new(a) if a[0].to_i == id and a.size == 4
      end
      return nil
    end

    def self.find_by_username username
      f = NailFucker::Base.open_account('r')
      while line = f.gets do
        a = line.split(':')
        return NailFucker::Account.new(a) if a[1] == username and a.size == 4
      end
      return nil
    end

    def save!
      NailFucker::Account.remove!(self) #remove self from file (with id which cannot be modified)
      NailFucker::Account.append!(self) #append updated self from file (with new values)
    end

    def to_s(n=:line)
      out = ""
      if n == :bloc
        out << "ID : " + @id.to_s
        out << "USERNAME : " + @username.to_s
        out << "ACCOUNT : " + (@count.to_f/100).to_s + " €"
      elsif n == :line
        out += "#" + @id.to_s.static(4) + "\t"
        out += @username.to_s.static(16) + "\t"
        out += (@count.to_f/100).to_s + " €"
      else
        self.to_s(0)
      end
      return out
    end

    def to_file
      @id.to_s + ":" + @username + ":" + @password + ":" + @count.to_s + "\n"
    end

    def self.all
      accounts = {}
      f = NailFucker::Base.open_account('r')
      while line = f.gets do
        a = line.split(':')
        accounts[a[0].to_i] = NailFucker::Account.new(a) if a.size == 4
      end
      f.close
      return accounts
    end

    def self.remove!(account)
      accounts = NailFucker::Account.all
      accounts.delete(account.id)
      f = NailFucker::Base.open_account('w')
      accounts.each do |k, v|
        f << v.to_file
      end
      f.close
    end

    def self.append!(account)
      f = NailFucker::Base.open_account('a')
      f << account.to_file
      f.close
    end

  end
end
