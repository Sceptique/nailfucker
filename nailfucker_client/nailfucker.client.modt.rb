#encoding: utf-8

require 'version'

module NailFucker
  class Client

    def get_modt_hash modt_array
      modt_hash = Hash.new
      modt_array.each do |line|
        if line.match(/\A([\w])+\:([\w\.])+\Z/)
          modt_hash[line.split(":")[0].to_sym] = line.split(":")[1]
        else
          modt_hash[:line] ||= String.new
          modt_hash[:line] << line
        end
      end
      return modt_hash
    end

    def get_modt
      modt_array = get_messages
      modt_hash = get_modt_hash(modt_array)
      version_server = modt_hash[:version_server].to_s.to_version
      version_protocol = modt_hash[:version_protocol].to_s.to_version
      puts "Server Version : " + version_server.to_s

      if version_protocol < NailFucker::META[:version_protocol_compatible]
        puts "Sorry, you do not have a compatible version"
        @s.puts "quit"
        exit(0)
      end

      puts "============"
      puts modt_hash[:line].to_s
      puts "============"
      puts
    end

  end
end
