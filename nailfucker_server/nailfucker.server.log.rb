#encoding: utf-8

module NailFucker
  class Server

    LOG_SERV = -1

    def log id, log_message
      type = "[NOTE]"
      type = "[USER]" if id.to_i >= 0
      type = "[SERV]" if id.to_i == -1
      date = Time.now.to_simple_date + " " + Time.now.to_simple_time
      log_line = type + " " + date + " ::" + id.to_s + ":: " + log_message.to_s << "\n"
      puts log_line
      NailFucker::Base.write_log(log_line)
    end

  end
end
