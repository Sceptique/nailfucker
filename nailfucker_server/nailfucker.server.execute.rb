#encoding: utf-8

module NailFucker
  class Server

    def execute_cmd_local_is_valid? cmd
      return :config if cmd.to_s.match(/\w+=\w+/i)
      return :cmd if COMMANDS_LOCAL.include?(cmd.split[0].to_s.to_sym)
      return false
    end

    def execute_cmd_local cmd
      type = execute_cmd_local_is_valid?(cmd)
      # TODO : homogeniser
      if type == :config
        config_set!(cmd.split("=")[0].to_sym, cmd.split("=")[1])
        log LOG_SERV, "Execute \"" + cmd + "\""
        return true
      elsif type == :cmd
        return self.send("cmd_local_#{cmd.split[0].to_s}", cmd)
      end
      return "failure"
    end

    def execute_cmd_client_is_valid? cmd
      return true if COMMANDS_CLIENT.include?(cmd.split[0].to_s.to_sym)
      return true if cmd.to_s.match(/\A(\+|\-)?(\d)+\Z/)
      return false
    end

    def execute_cmd_client client, user, cmd
      type = execute_cmd_client_is_valid?(cmd)
      #execution si le type est ok
      if type
        if err = self.send("cmd_client_#{cmd.split[0].to_s}", user, cmd)
          err.to_s.split("\n").each do |out|
            client.puts out
          end
          client.puts ">end<"
          return true
        end
      else
        client.puts "failure"
        client.puts ">end<"
        return false
      end
    end

  end
end
