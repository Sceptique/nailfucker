#encoding: utf-8

module NailFucker
  class Server

    #ecrase the old data
    def config_load(file=nil)
      @config.merge(config_preset(NailFucker::Base.read_config(file)))
    end

    def config_load!(file=nil)
      @config = config_load(file)
    end

    def config_reset!
      @config = {}
    end

    #returns the whole config
    def config_set key, value
      config = @config.merge({key.to_sym => value})
      config_preset(config)
    end

    def config_set! key, value
      @config = config_set(key, value)
    end

    private
    def config_preset config
      #Open/Close registrations
      config[:registration] ||= true
      config[:registration] = config[:registration].to_s.to_t
      return config
    end

  end
end
